# Insta Rec

Recommender system for Instagram images and captions.

## Installation

 *  Install the lib `pip install git+https://gitlab.com/joao_fidalgo/insta_rec.git`
 *  Download the jupyter notebook directory [insta_rec](https://drive.google.com/file/d/1roOoAOuJsHsPj5lXu9ChnQb5QJNGgGa9/view?usp=sharing) (includes small image repository)
 *  Uncompress downloaded jupyter notebook directory
 *  Run the jupyter notebook *insta_rec.ipynb* that is inside the jupyter notebook directory downloaded
 *  Have fun!

## Author
João Fidalgo
