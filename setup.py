from setuptools import setup, find_packages

PACKAGE = "insta_rec"

setup(
    name=PACKAGE,
    version='1.0.0.26',
    packages=find_packages(),
    url='',
    license='',
    author='João Fidalgo',
    author_email='joao.fidalgo.89@gmail.com',
    description='Recommender system for Instagram images and captions.',
)