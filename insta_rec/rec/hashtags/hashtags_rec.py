from insta_rec.rec.basic.basic_rec import BasicRecommender
from insta_rec.rec_steps.candidate_picker.candidate_picker_all import CandidatePickerAll
from insta_rec.rec_steps.cadidate_mapper.candidate_mapper_from_npy import CandidateMapperFromNPY
from insta_rec.rec_steps.subject_generator.subject_generator_from_npy import SubjectGeneratorFromNPY
from insta_rec.rec_steps.scorer.cosine_distance import CosineDistance
from insta_rec.visualizer.vizualizer_hashtags import HashtagVisualizer
from insta_rec.visualizer.visualizer_images import ImagesVisualizer


PATH_HASHTAGS_FEATURES = f'features/hashtags_DenseNet201.npy'
PATH_HASHTAGS_SKS = f'paths/hashtag_sks.p'
PATH_IMAGES_FEATURES = f'features/DenseNet201.npy'
PATH_IMAGES_SKS = f'paths/candidate_sks.p'
PATH_IMAGES = f'paths/images.p'


class HashtagsRec(BasicRecommender):
    def __init__(self, path_hashtags_features=PATH_HASHTAGS_FEATURES,
                 path_hashtags_sks=PATH_HASHTAGS_SKS,
                 path_images_features=PATH_IMAGES_FEATURES,
                 path_images_sks=PATH_IMAGES_SKS,
                 path_images=PATH_IMAGES):
        candidate_picker = CandidatePickerAll(path_hashtags_sks, exclude_self=False)
        candidate_mapper = CandidateMapperFromNPY(path_hashtags_features, path_hashtags_sks)
        subject_generator = SubjectGeneratorFromNPY(path_images_features, path_images_sks)
        scorer = CosineDistance()
        rescorer = None
        visualizer = HashtagVisualizer()
        images_visualizer = ImagesVisualizer(path_images)
        super().__init__(candidate_picker, candidate_mapper, subject_generator, scorer, rescorer, visualizer, images_visualizer)

    def visualize_subject(self, subject_sk):
        if self.images_visualizer is not None:
            return self.images_visualizer.visualize([subject_sk])
