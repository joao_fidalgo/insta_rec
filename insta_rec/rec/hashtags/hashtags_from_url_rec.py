from insta_rec.rec.basic.basic_rec import BasicRecommender
from insta_rec.rec_steps.candidate_picker.candidate_picker_all import CandidatePickerAll
from insta_rec.rec_steps.cadidate_mapper.candidate_mapper_from_npy import CandidateMapperFromNPY
from insta_rec.rec_steps.subject_generator.visual_features_from_url_subject_generator import VisualFeaturesFromURLSubjectGenerator
from insta_rec.rec_steps.scorer.cosine_distance import CosineDistance
from insta_rec.visualizer.vizualizer_hashtags import HashtagVisualizer
from urllib.request import urlopen
from PIL import Image


PATH_HASHTAGS_FEATURES = f'features/hashtags_DenseNet201.npy'
PATH_HASHTAGS_SKS = f'paths/hashtag_sks.p'


class HashtagsFromURLRec(BasicRecommender):
    def __init__(self, path_hashtags_features=PATH_HASHTAGS_FEATURES,
                 path_hashtags_sks=PATH_HASHTAGS_SKS):
        candidate_picker = CandidatePickerAll(path_hashtags_sks, exclude_self=False)
        candidate_mapper = CandidateMapperFromNPY(path_hashtags_features, path_hashtags_sks)
        subject_generator = VisualFeaturesFromURLSubjectGenerator()
        scorer = CosineDistance()
        rescorer = None
        visualizer = HashtagVisualizer()
        super().__init__(candidate_picker, candidate_mapper, subject_generator, scorer, rescorer, visualizer)

    def visualize_subject(self, subject_identifier):
        return Image.open(urlopen(subject_identifier))
