from insta_rec.rec.basic.basic_rec import BasicRecommender
from insta_rec.rec_steps.candidate_picker.candidate_picker_all import CandidatePickerAll
from insta_rec.rec_steps.cadidate_mapper.candidate_mapper_from_npy import CandidateMapperFromNPY
from insta_rec.rec_steps.subject_generator.visual_features_from_url_subject_generator import VisualFeaturesFromURLSubjectGenerator
from insta_rec.rec_steps.scorer.cosine_distance import CosineDistance
from insta_rec.visualizer.visualizer_images import ImagesVisualizer
from urllib.request import urlopen
from PIL import Image


PATH_IMAGES_FEATURES = f'features/DenseNet201.npy'
PATH_IMAGES_SKS = f'paths/candidate_sks.p'
PATH_IMAGES = f'paths/images.p'


class VisualSimilarityFromURLRec(BasicRecommender):
    def __init__(self, path_images_features=PATH_IMAGES_FEATURES,
                 path_images_sks=PATH_IMAGES_SKS,
                 path_images=PATH_IMAGES):
        candidate_picker = CandidatePickerAll(path_images_sks, exclude_self=False)
        candidate_mapper = CandidateMapperFromNPY(path_images_features, path_images_sks)
        subject_generator = VisualFeaturesFromURLSubjectGenerator()
        scorer = CosineDistance()
        rescorer = None
        visualizer = ImagesVisualizer(path_images)
        super().__init__(candidate_picker, candidate_mapper, subject_generator, scorer, rescorer, visualizer)

    def visualize_subject(self, subject_identifier):
        return Image.open(urlopen(subject_identifier))
