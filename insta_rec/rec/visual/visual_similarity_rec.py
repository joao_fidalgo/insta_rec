from insta_rec.rec.basic.basic_rec import BasicRecommender
from insta_rec.rec_steps.candidate_picker.candidate_picker_all import CandidatePickerAll
from insta_rec.rec_steps.cadidate_mapper.candidate_mapper_from_npy import CandidateMapperFromNPY
from insta_rec.rec_steps.subject_generator.subject_generator_from_npy import SubjectGeneratorFromNPY
from insta_rec.rec_steps.scorer.cosine_distance import CosineDistance
from insta_rec.visualizer.visualizer_images import ImagesVisualizer


PATH_CANDIDATES = f'features/DenseNet201.npy'
PATH_CANDIDATES_SKS = f'paths/candidate_sks.p'
PATH_IMAGES = f'paths/images.p'


class VisualSimilarityRec(BasicRecommender):
    def __init__(self, path_candidates=PATH_CANDIDATES,
                 path_candidate_sks=PATH_CANDIDATES_SKS,
                 path_images=PATH_IMAGES):
        candidate_picker = CandidatePickerAll(path_candidate_sks)
        candidate_mapper = CandidateMapperFromNPY(path_candidates, path_candidate_sks)
        subject_generator = SubjectGeneratorFromNPY(path_candidates, path_candidate_sks)
        scorer = CosineDistance()
        rescorer = None
        visualizer = ImagesVisualizer(path_images)
        super().__init__(candidate_picker, candidate_mapper, subject_generator, scorer, rescorer, visualizer)
