class BasicRecommender:
    def __init__(self, candidate_picker, candidate_mapper,
                 subject_generator, scorer, rescorer=None,
                 visualizer=None, images_visualizer=None):
        self.candidate_picker = candidate_picker
        self.candidate_mapper = candidate_mapper
        self.subject_generator = subject_generator
        self.scorer = scorer
        self.rescorer = rescorer
        self.visualizer = visualizer
        self.images_visualizer = images_visualizer

    def load(self):
        for rec_step in [self.candidate_picker,
                         self.candidate_mapper,
                         self.subject_generator,
                         self.scorer,
                         self.rescorer,
                         self.visualizer,
                         self.images_visualizer]:
            if rec_step is not None:
                rec_step.load()

    def visualize_subject(self, subject_sk):
        if self.visualizer is not None:
            return self.visualizer.visualize([subject_sk])

    def recommend(self, subject_sk, n_rec=8, n_re_rec=8, visualize=True):
        candidate_sks = self.candidate_picker.get(subject_sk)
        candidates = self.candidate_mapper.map(candidate_sks)
        subject = self.subject_generator.generate(subject_sk)
        recommended_sks, scores = self.scorer.score(subject, candidate_sks, candidates, n_rec)
        if self.rescorer is not None:
            recommended_sks, scores = self.rescorer.rescore(subject, recommended_sks, candidates, scores, n_re_rec)
        if visualize and self.visualizer is not None:
            return self.visualizer.visualize(recommended_sks)
        else:
            return recommended_sks, scores
