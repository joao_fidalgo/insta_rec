from insta_rec.rec.basic.basic_rec import BasicRecommender
from insta_rec.rec_steps.candidate_picker.dummy import DummyCandidatePicker
from insta_rec.rec_steps.cadidate_mapper.dummy import DummyCandidateMapper
from insta_rec.rec_steps.subject_generator.dummy import DummySubjectGenerator
from insta_rec.rec_steps.scorer.dummy import DummyScorer
from insta_rec.rec_steps.rescorer.dummy import DummyRescorer
from insta_rec.visualizer.visualizer_images import ImagesVisualizer


PATH_IMAGES = f'paths/images.p'


class DummyRec(BasicRecommender):
    def __init__(self, path_images=PATH_IMAGES):
        candidate_picker = DummyCandidatePicker()
        candidate_mapper = DummyCandidateMapper()
        subject_generator = DummySubjectGenerator()
        scorer = DummyScorer()
        rescorer = DummyRescorer()
        if path_images is not None:
            visualizer = ImagesVisualizer(path_images)
        else:
            visualizer = None
        super().__init__(candidate_picker, candidate_mapper, subject_generator, scorer, rescorer, visualizer)
