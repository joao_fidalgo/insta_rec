import tensorflow as tf
from urllib.request import urlopen
import numpy as np
from PIL import Image


class VisualFeaturesFromURLSubjectGenerator:
    def __init__(self, image_size=(300, 300)):
        self.model = None
        self.preprocess = None
        self.image_size = image_size

    def load(self):
        self.model = tf.keras.applications.densenet.DenseNet201(include_top=False,
                                                                weights='imagenet',
                                                                pooling='avg')
        self.preprocess = tf.keras.applications.densenet.preprocess_input

    def generate(self, image_url):
        image_arr = np.array(Image.open(urlopen(image_url)))
        image_arr = tf.image.resize(image_arr, self.image_size)
        image_arr = np.expand_dims(image_arr, 0)
        image_arr_writable = np.zeros(image_arr.shape)
        image_arr_writable[:] = image_arr
        image_arr = image_arr_writable
        image_arr = self.preprocess(image_arr)
        return self.model.predict(image_arr).reshape(1, -1)
