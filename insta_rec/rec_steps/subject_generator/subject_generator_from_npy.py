import numpy as np
import pandas as pd


class SubjectGeneratorFromNPY:
    def __init__(self, path_candidates, path_candidate_sks):
        self.candidates = None
        self.candidate_sk_to_candidate = None
        self.path_candidates = path_candidates
        self.path_candidate_sks = path_candidate_sks

    def load(self):
        self.candidates = np.load(self.path_candidates, mmap_mode='r')
        candidate_sks = pd.read_pickle(self.path_candidate_sks)
        self.candidate_sk_to_candidate = pd.Series(candidate_sks.index.values,
                                                   index=candidate_sks)

    def generate(self, subject_sk):
        return np.expand_dims(self.candidates[self.candidate_sk_to_candidate[subject_sk]], 0)
