import numpy as np


class DummySubjectGenerator:
    def __init__(self):
        pass

    @staticmethod
    def load():
        pass

    @staticmethod
    def generate(subject_sk):
        return subject_sk
