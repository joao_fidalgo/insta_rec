import numpy as np


class DummyScorer:
    def __init__(self):
        pass

    @staticmethod
    def load():
        pass

    @staticmethod
    def score(subject, candidate_sks, candidates, n_rec):
        recommended_sks = np.repeat(candidate_sks, n_rec)
        scores = [1] * n_rec
        return recommended_sks, scores
