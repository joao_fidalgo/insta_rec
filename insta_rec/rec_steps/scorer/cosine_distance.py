import pandas as pd
import numpy as np


class CosineDistance:
    def __init__(self):
        pass

    def load(self):
        pass

    def score(self, subject, candidate_sks, candidates, n_rec):
        cos_similarity = self.calculate_cos_similarity(candidates, subject)
        scores_series = cos_similarity.nlargest(n_rec)
        scores = scores_series.values
        most_similar_idx = list(scores_series.index)
        recommended_sks = candidate_sks[most_similar_idx].values
        return recommended_sks, scores

    @staticmethod
    def calculate_cos_similarity(candidates, subject):
        denominator = np.dot(candidates, subject.T)
        numerator = (np.sqrt(np.sum(candidates ** 2, axis=1, keepdims=True))
                     * np.sqrt(np.sum(subject ** 2, axis=1, keepdims=True)))
        cos_similarity = denominator / numerator
        cos_similarity = pd.Series(cos_similarity.T[0])
        # remove repeated images (there may be repeated images in the dataset)
        cos_similarity = cos_similarity[cos_similarity != 1]
        return cos_similarity
