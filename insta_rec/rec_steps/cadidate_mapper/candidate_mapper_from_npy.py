import numpy as np
import pandas as pd


class CandidateMapperFromNPY:
    def __init__(self, path_candidates, path_candidate_sks):
        self.candidates = None
        self.candidate_sks = None
        self.path_candidates = path_candidates
        self.path_candidate_sks = path_candidate_sks

    def load(self):
        self.candidates = np.load(self.path_candidates, mmap_mode='r')
        self.candidate_sks = pd.read_pickle(self.path_candidate_sks)

    def map(self, candidate_sks):
        return self.candidates[self.candidate_sks.isin(candidate_sks)]
