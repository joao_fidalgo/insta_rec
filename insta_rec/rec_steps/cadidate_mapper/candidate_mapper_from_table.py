import pandas as pd


class CandidateMapperFromTable:
    def __init__(self, path_candidates):
        self.candidates = None
        self.path_candidates = path_candidates

    def load(self):
        self.candidates = pd.read_pickle(self.path_candidates)

    def map(self, candidate_sks):
        return self.candidates.loc[candidate_sks].values
