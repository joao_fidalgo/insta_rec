class DummyRescorer:
    def __init__(self):
        pass

    @staticmethod
    def load():
        pass

    @staticmethod
    def rescore(subject, recommended_sks, candidates, scores, n_re_rec):
        return recommended_sks, scores
