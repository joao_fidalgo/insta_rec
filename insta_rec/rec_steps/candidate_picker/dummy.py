import numpy as np


class DummyCandidatePicker:
    def __init__(self):
        pass

    @staticmethod
    def load():
        pass

    @staticmethod
    def get(subject_sk):
        return np.array([subject_sk])
