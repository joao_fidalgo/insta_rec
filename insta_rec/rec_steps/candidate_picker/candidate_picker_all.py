import pandas as pd


class CandidatePickerAll:
    def __init__(self, path_candidate_sks, exclude_self=True):
        self.candidate_sks = None
        self.path_candidate_sks = path_candidate_sks
        self.exclude_self = exclude_self

    def load(self):
        self.candidate_sks = pd.read_pickle(self.path_candidate_sks)

    def get(self, subject_sk):
        if self.exclude_self:
            return self.candidate_sks[self.candidate_sks != subject_sk].reset_index(drop=True)
        else:
            return self.candidate_sks
