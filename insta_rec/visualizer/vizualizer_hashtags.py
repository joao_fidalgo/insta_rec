class HashtagVisualizer:
    def __init__(self):
        pass

    @staticmethod
    def load():
        pass

    @staticmethod
    def visualize(hashtags):
        return ' '.join(hashtags)
