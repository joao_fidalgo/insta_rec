import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import math
from insta_rec.rec_steps.cadidate_mapper.candidate_mapper_from_table import CandidateMapperFromTable


class ImagesVisualizer:
    def __init__(self, path_images):
        if path_images is None:
            raise ValueError('path_images is None')
        self.path_images = path_images
        self.image_mapper = CandidateMapperFromTable(self.path_images)

    def load(self):
        self.image_mapper.load()

    def visualize(self, sks):
        n_posts = len(sks)
        n_cols = min(4, n_posts)
        n_rows = math.ceil(n_posts / n_cols)
        images = self.image_mapper.map(sks)
        fig = plt.figure(figsize=(5 * n_cols, 5 * n_rows))
        for i in range(n_posts):
            ax = plt.subplot(n_rows, n_cols, i + 1)
            _ = plt.imshow(mpimg.imread(images[i]))
            _ = plt.axis("off")
        plt.close(fig)
        return fig
